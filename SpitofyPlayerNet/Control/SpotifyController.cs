﻿using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyPlayerNet.Control
{
    public class SpotifyController
    {
        private SpotifyWebAPI _spotify;

        public string Token { get { return _spotify?.AccessToken; } }

        public SpotifyController()
        {
            Task.Run(() => RunAuthentication());
        }

        private static SpotifyController _instance;

        public static SpotifyController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SpotifyController();

                return _instance;
            }
        }

        public async void GetToken()
        {

            await RunAuthentication();

        }

        private async Task RunAuthentication()
        {
            WebAPIFactory webApiFactory = new WebAPIFactory(
                "http://localhost",
                8000,
                "26d287105e31491889f3cd293d85bfea",
                Scope.UserReadPrivate | Scope.UserReadEmail | Scope.PlaylistReadPrivate | Scope.UserLibraryRead |
                Scope.UserReadPrivate | Scope.UserFollowRead | Scope.UserReadBirthdate | Scope.UserTopRead | Scope.PlaylistReadCollaborative |
                Scope.UserReadRecentlyPlayed | Scope.UserReadPlaybackState | Scope.UserModifyPlaybackState);

            try
            {
                _spotify = await webApiFactory.GetWebApi();
            }
            catch (Exception ex)
            {
            }

            if (_spotify == null)
                return;

        }
    }
}
