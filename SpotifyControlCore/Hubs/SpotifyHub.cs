﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotifyControlCore.Hubs
{
    public class SpotifyHub : Hub
    {
        public async Task EncolarCancion(string urlSong,string nombre,string artista)
        {
            await Clients.All.SendAsync("EncolarCancion", urlSong,nombre,artista);
        }

        public async Task CambiarCancion(string nombreCancion)
        {
            await Clients.All.SendAsync("CambiarCancion", nombreCancion);
        }
    }
}
