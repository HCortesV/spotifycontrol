﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SpotifyControlCore.Control;
using SpotifyControlCore.Models;

namespace SpotifyControlCore.Controllers
{
    public class HomeController : Controller
    {
        private IConfiguration _configuration;
        private string _token; 

        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
            //var _token = SpotifyWeb.Instance.Token;
        }

        public IActionResult Index()
        {
            var spotifySection = _configuration.GetSection("Spotify").GetSection("Redirect");
            ViewBag.AuthUrl = Spotify.Instance.GetAuthUrl("http://10.228.4.38:5000/Home/AuthResponse/");
            return View();
        }

        [HttpGet]
        public IActionResult Search(string query)
        {
            var spotify = new Spotify();
            var items = spotify.BuscarCancion(query, Spotify.Instance.Token);
            return Json(items);
        }

        public IActionResult AuthResponse(string access_token, string token_type, string expires_in, string state)
        {
            if(!string.IsNullOrEmpty(access_token))
                Spotify.Instance.Token = access_token;
            return View();
        }

        [HttpPost]
        public IActionResult CambiarCancion(string cancion)
        {
            return Json("");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
