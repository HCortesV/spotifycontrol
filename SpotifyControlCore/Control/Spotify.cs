﻿using SpotifyAPI.Web.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotifyControlCore.Control
{
    public class Spotify
    {
        private string url = "https://api.spotify.com/v1/";

        private static Spotify _instance;

        public string Token { get; set; }

        public static Spotify Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Spotify();

                return _instance;
            }
        }


        public string GetAuthUrl(string redirectUri)
        {
            string clientId = "9e7c21a8f0d04e4f8c72cbb875777705";
            string urlSpotify = "https://accounts.spotify.com/en/authorize?";

            var scopes = "playlist-read-private playlist-modify-private user-read-private user-read-email";

            return urlSpotify +
                   "client_id="+clientId+
                   "&response_type=token"+
                   "&redirect_uri="+redirectUri+
                   "&state=&scope="+scopes+
                   "&show_dialog=true";
        }

        public IEnumerable<SpotifyAPI.Web.Models.FullTrack> BuscarCancion(string query,string token)
        {
            var client = new RestSharp.RestClient(url);

            var request = new RestSharp.RestRequest("search", RestSharp.Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddParameter("q", query, RestSharp.ParameterType.QueryString); 
            request.AddParameter("type", "track", RestSharp.ParameterType.QueryString);

            var items = Newtonsoft.Json.JsonConvert.DeserializeObject<SpotifyAPI.Web.Models.SearchItem>(client.Execute(request).Content);

            return items.Tracks.Items;
        }
    }
}
