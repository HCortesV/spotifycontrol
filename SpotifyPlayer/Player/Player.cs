﻿using Microsoft.AspNet.SignalR.Client;
using SpotifyAPI;
using SpotifyAPI.Local;
using SpotifyAPI.Local.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpotifyPlayer.Player
{
    public class Player
    {
        private readonly SpotifyLocalAPIConfig _config;
        private SpotifyLocalAPI _spotify;
        private Track _currentTrack;
        private IHubProxy _hub;
        private Queue<string> _queue;

        public Player(IHubProxy hub, ref Queue<string> queue)
        {
            _config = new SpotifyLocalAPIConfig
            {
                ProxyConfig = new ProxyConfig()
            };
            //_hub = hub;
            _queue = queue;
            _spotify = new SpotifyLocalAPI(_config);
            _spotify.OnPlayStateChange += _spotify_OnPlayStateChange;
            Connect();
        }

        public void Connect()
        {
            if (!SpotifyLocalAPI.IsSpotifyRunning())
            {
                return;
            }
            if (!SpotifyLocalAPI.IsSpotifyWebHelperRunning())
            {
                return;
            }

            bool successful = _spotify.Connect();
            if (successful)
            {
                _spotify.ListenForEvents = true;
            }
            else
            {
                throw new Exception("Hay que abrir spotify");
            }   
        }

        private void _spotify_OnPlayStateChange(object sender, PlayStateEventArgs e)
        {
            Console.Write("cambio");
        }
        
    }
}
