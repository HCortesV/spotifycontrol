﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpotifyPlayer
{
    public partial class VentanaPrincipal : Form
    {
        private Player.Player _player;
        private IHubProxy myHub;
        private HubConnection connection;
        private Queue<string> colaCanciones;

        public VentanaPrincipal()
        {
            InitializeComponent();
            colaCanciones = new Queue<string>();
            ConectarHub();
            _player = new Player.Player(myHub,ref colaCanciones);
        }

        public async Task ConectarHub()
        {
            try
            {
                connection = new HubConnection(ConfigurationManager.AppSettings["url"]??"");

                

                myHub = connection.CreateHubProxy(ConfigurationManager.AppSettings["hub"] ?? "");

                await connection.Start().ContinueWith(task => {
                    if (task.IsFaulted)
                    {
                        MessageBox.Show(String.Format("There was an error opening the connection:{0}",
                                          task.Exception.GetBaseException()));
                    }
                    else
                    {
                        MessageBox.Show("Connected");
                    }

                });


                myHub.On<string>("EncolarCancion", param =>
                {
                    colaCanciones.Enqueue(param);
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            connection.Stop();
        }

    }
}
