﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SpotifyControlOwin.Hubs
{
    public class SpotifyControlHub : Hub
    {
        public void EncolarCancion(string urlSong, string nombre, string artista)
        {
            Clients.All.SendAsync("EncolarCancion", urlSong, nombre, artista);
        }

        public void CambiarCancion(string nombreCancion)
        {
            Clients.All.SendAsync("CambiarCancion", nombreCancion);
        }
    }
}