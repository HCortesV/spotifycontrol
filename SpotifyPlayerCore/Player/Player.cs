﻿using Microsoft.AspNetCore.SignalR.Client;
using SpotifyAPI;
using SpotifyAPI.Local;
using SpotifyAPI.Local.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpotifyPlayerCore.Player
{
    public class Player
    {
        private readonly SpotifyLocalAPIConfig _config;
        private SpotifyLocalAPI _spotify;
        private HubConnection _connection;
        private Queue<string> _queue;
        private ManualResetEvent _waitHandle;

        public Player(HubConnection connection, ref Queue<string> queue)
        {
            _config = new SpotifyLocalAPIConfig
            {
                ProxyConfig = new ProxyConfig()
            };
            _connection = connection;
            _queue = queue;
            _spotify = new SpotifyLocalAPI(_config);
            _spotify.OnTrackChange += _spotify_OnPlayStateChange;
            _spotify.OnPlayStateChange += _spotify_OnStateChange;
            _spotify.OnVolumeChange += _spotify_OnVolumeChange;

        }

        public void Connect()
        {
            if (!SpotifyLocalAPI.IsSpotifyRunning())
            {
                Console.WriteLine("No esta corriendo spotify");
                return;
            }
            if (!SpotifyLocalAPI.IsSpotifyWebHelperRunning())
            {
                Console.WriteLine("No esta corriendo spotify webhelper");
                return;
            }

            bool successful = _spotify.Connect();
            if (successful)
            {
                Console.WriteLine("Conectado a spotify");
                _spotify.ListenForEvents = true;
            }
            else
            {
                throw new Exception("Hay que abrir spotify");
            }   
        }

        private void _spotify_OnVolumeChange(object sender, VolumeChangeEventArgs e)
        {
            Console.WriteLine("Cambio volumen");
        }

        private void _spotify_OnPlayStateChange(object sender, TrackChangeEventArgs e)
        {
            Console.WriteLine("Escuchando: " + e.NewTrack.TrackResource.Name); 
        }

        private void _spotify_OnStateChange(object sender, PlayStateEventArgs e)
        {
            
            if (_spotify.GetStatus().Playing) {
                Console.WriteLine("Se esta escuchando");
                return;
            }
            if (!_queue.Any())
            {
                Console.WriteLine("No hay canciones en cola");
                return;
            }
            var song = _queue.Dequeue();
            _spotify.PlayURL(song);
            _connection.InvokeAsync<string>("CambiarCancion", song).Wait();
        }

        public void EscucharPrimeraCancion()
        {
            if (!_queue.Any()) return;
            if (_spotify.GetStatus().Playing) return;

            var song = _queue.Dequeue();
            _spotify.PlayURL(song);
        }
        
    }
}
