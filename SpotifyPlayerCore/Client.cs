﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpotifyPlayerCore
{
    public class Client
    {

        private HubConnection _connection;
        private Player.Player _player;
        private Queue<string> _colaCanciones;
        private ManualResetEvent _waitHandle;

        public Client()
        {
            Conectar();
            _colaCanciones = new Queue<string>();
        }

        public void Player()
        {
            _player = new Player.Player(_connection, ref _colaCanciones);
            _player.Connect();
        }
        
        private void Conectar()
        {
            _connection = new HubConnectionBuilder()
               .WithUrl("http://10.228.4.238:5000/hubs/spotifyhub")
               .Build();


            _connection.On<string,string,string>("EncolarCancion", (url,nombre,artista) =>
            {
                Console.WriteLine("Cancion Encolada: {0} del artista {1}",nombre,artista );
                _colaCanciones.Enqueue(url);

                _player.EscucharPrimeraCancion();

            });

            _connection.StartAsync().Wait();
        }
    }
}
