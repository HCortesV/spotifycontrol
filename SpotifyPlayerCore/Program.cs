﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SpotifyPlayerCore
{
    class Program
    {
        //private static ManualResetEvent waitHandle = new ManualResetEvent(false);
        static void Main(string[] args)
        {
            var client = new Client();
            client.Player();
            Thread.Sleep(-1);
        }
    }
}
