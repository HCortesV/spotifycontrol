﻿using SpotifyControl.Control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SpotifyControl.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.AuthUrl = Spotify.Instance.GetAuthUrl();
            return View();
        }

        [HttpGet]
        public ActionResult Search(string query)
        {
            var items = Spotify.Instance.BuscarCancion(query, Spotify.Instance.Token);
            return Json(items);
        }

        [HttpGet]
        public ActionResult AuthResponse(string access_token, string token_type, string expires_in, string state)
        {
            if (!string.IsNullOrEmpty(access_token))
                Spotify.Instance.Token = access_token;
            return View();
        }

        [HttpPost]
        public ActionResult CambiarCancion(string cancion)
        {
            return Json("");
        }
    }
}