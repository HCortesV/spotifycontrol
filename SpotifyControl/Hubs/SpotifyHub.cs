﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpotifyControl.Hubs
{
    public class SpotifyHub : Hub
    {
        public void EncolarCancion(string urlSong,string nombre,string artista)
        {
            Clients.All.SendAsync("EncolarCancion", urlSong,nombre,artista);
        }

        public void CambiarCancion(string nombreCancion)
        {
            Clients.All.SendAsync("CambiarCancion", nombreCancion);
        }
    }
}
